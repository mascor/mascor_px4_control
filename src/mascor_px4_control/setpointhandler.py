# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import threading
import numpy as np

import rospy
import tf
import tf2_ros
import tf2_geometry_msgs

## -- Messages
from std_msgs.msg import Header
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Point
from geometry_msgs.msg import Vector3
from mavros_msgs.msg import PositionTarget

## -- Services
from std_srvs.srv import SetBool, SetBoolResponse

class SetpointHandler:
    """Handler for setpoints in offboard flightmode.

    Attributes:
        ns              (str): mavros instance namespace
        localization (object): instance of LocalizationHandler()
        logger       (object): instance of LogHandler()
    """
    def __init__(self, ns, localization, logger):
        ## -- Variables
        self.__namespace = ns
        self.localization = localization
        self.logger = logger

        ## -- Thread
        self.lock_sp_msg = threading.Lock()
        self.__kill_thread = False
        self.__thread_killed = False
        self.__thread_started = False
        ## -- Setpoint RAW msg
        self.__setpoint_msg = PositionTarget(
                            header=Header(stamp=rospy.Time.now(), frame_id=self.localization.get_ref_frame()),
                            type_mask=self.get_mask_pos_vel(),
                            coordinate_frame=PositionTarget().FRAME_LOCAL_NED,
                            position=Point(x=0., y=0., z=1.5),
                            velocity=Vector3(x=0., y=0., z=0.))
        ## -- TF
        self.__tfBuffer = tf2_ros.Buffer()
        self.__tfListener = tf2_ros.TransformListener(self.__tfBuffer)
        self._br = tf2_ros.TransformBroadcaster()
        ## -- Publishers
        self.__setpoint_raw_pub = rospy.Publisher(self.__namespace + '/mavros/setpoint_raw/local', PositionTarget, queue_size=1)
        ## -- Services
        rospy.Service(self.__namespace + '/mavros/setpoint_lock', SetBool, self.__handle_sp_lock)

    ## -- SERVICES
    def __handle_sp_lock(self, req):
        """Service handler for pause/resume publishing setpoint messages.
        Necessary if you want to use this library and another 
        setpoint publisher in different node.
        """
        if req.data:
            self.lock_sp_msg.acquire()
            return SetBoolResponse(True, "Success")
        else:
            try:
                self.lock_sp_msg.release()
            except:
                self.logger.logerr("Lock already unlocked.")
            return SetBoolResponse(True, "Success")

    ## -- THREAD
    def __setpoint_raw_publisher(self):
        """Thread for setpoint raw msg publisher
        """
        rate = rospy.Rate(25)
        self.__thread_started = True
        while not self.__kill_thread:
            self.lock_sp_msg.acquire()
            msg = self.__setpoint_msg
            self.lock_sp_msg.release()
            msg.header.stamp = rospy.Time.now()
            try:
                self.__setpoint_raw_pub.publish(msg)
            except:
                self.logger.logerr("SetpointPublisher failed.")
                pass
            rate.sleep()
        self.__thread_killed = True
        self.__thread_started = False

    def start_thread(self):
        """Starts Thread __setpoint_raw_publisher()
        """
        if not self.__thread_started:
            setpointPublisher = threading.Thread(target=self.__setpoint_raw_publisher)
            setpointPublisher.daemon = True
            setpointPublisher.start()
    
    def kill_thread(self):
        """Kills the thread
        """
        self.__kill_thread = True
        rate = rospy.Rate(10)
        while not self.__thread_killed:
            rate.sleep()

    ## -- OFFBOARD CONTROL
    def hold(self):
        """Set actual position for Offboard mode.
        """
        pos = self.localization.get_position()
        yaw = self.localization.get_yaw()
        self.position(pos, yaw)

    def gps(self, lat, lon, z, yaw=0.):
        """Set GPS for Offboard mode.
        
        Arguments:
            lat (float): latitude  (degree)
            lon (float): longitude (degree)
            z   (float): altitude  (m)
            yaw (float): yaw       (rad)
        """
        xy = self.localization.get_position_from_lat_lon(lat, lon)
        xyz = [xy[0], xy[1], z]
        self.position(xyz, yaw)

    def position(self, position, yaw=0., ref_frame=None):
        """Set Position for Offboard mode.

        Arguments:
            position   (list): Array of x,y,z in reference frame (m)
            yaw       (float): yaw(rad) in reference frame (rad)
            accuracy  (float): sets accuracy for setpoint (m)
            ref_frame   (str): name of reference frame
        """
        if ref_frame is not None:
            pose_stamped = self.generate_pose_stamped(position, yaw, ref_frame)
            tf_pose = self.transform(pose_stamped, self.localization.get_ref_frame())
            position = [tf_pose.pose.position.x, tf_pose.pose.position.y, tf_pose.pose.position.z]
            quat = [tf_pose.pose.orientation.x, tf_pose.pose.orientation.y,
                    tf_pose.pose.orientation.z, tf_pose.pose.orientation.w]
            yaw = tf.transformations.euler_from_quaternion(quat)[2]
        self.lock_sp_msg.acquire()
        self.__setpoint_msg.position = Point(*position)
        self.__setpoint_msg.type_mask = self.get_mask_pos()
        self.__setpoint_msg.coordinate_frame = self.__setpoint_msg.FRAME_LOCAL_NED
        self.__setpoint_msg.yaw = yaw
        self.lock_sp_msg.release()


    def velocity(self, velocity, z=0.0, yaw=0.0, yaw_rate=0.0, frame=PositionTarget().FRAME_LOCAL_NED):
        """Set Velocity for Offboard mode.

        Arguments:
            velocity (numpy.ndarray): vx, vy, vz   (m/s)
            z                (float): altitude   (m)
            yaw              (float): angle      (rad)
            yaw_rate         (float): angle rate (rad/s)
            frame              (int): FRAME_LOCAL_NED = 1
                                      FRAME_BODY_NED = 8
        """
        #ROS BODY TO ENU BODY CONVERSION
        if frame == PositionTarget().FRAME_BODY_NED:
            vel_copy = np.copy(velocity)
            velocity[0] = -vel_copy[1]
            velocity[1] = vel_copy[0]
        #make msg write lock safe
        self.lock_sp_msg.acquire()

        #generate raw msg mask
        if yaw != 0.0:
            yaw_rate_mask = False
        elif yaw_rate != 0.0:
            yaw_rate_mask = True
        else:
            yaw_rate_mask = False
        if z != 0.0:
            self.__setpoint_msg.type_mask = self.get_mask_vel(z_velocity=False, yaw_rate=yaw_rate_mask) # vx, vy, z, yaw(_rate)
            self.__setpoint_msg.position.z = z
        else:
            self.__setpoint_msg.type_mask = self.get_mask_vel(z_velocity=True, yaw_rate=yaw_rate_mask) # vx, vy, vz, yaw(_rate)
        #write rest of message
        self.__setpoint_msg.coordinate_frame = frame
        self.__setpoint_msg.velocity = Vector3(*velocity)
        self.__setpoint_msg.yaw = yaw
        self.__setpoint_msg.yaw_rate = yaw_rate
        self.lock_sp_msg.release()


    def position_and_velocity(self, position, velocity, yaw=0.0, ref_frame=None):
        """Set Position with velocity as feedforward for Offboard mode.

        Arguments:
            position (numpy.ndarray): x,y,z array (m).
            velocity (numpy.ndarray): vx,vy,vz array (m/s).
            yaw              (float): yaw (rad) in reference frame.
        """
        self.lock_sp_msg.acquire()
        self.__setpoint_msg.type_mask = self.get_mask_pos_vel()
        self.__setpoint_msg.coordinate_frame = self.__setpoint_msg.FRAME_LOCAL_NED
        if ref_frame is not None:
            pose_stamped = self.generate_pose_stamped(position, yaw, ref_frame)
            try:
                trans_pose = self.transform(pose_stamped, self.localization.get_ref_frame())
                position = [trans_pose.pose.position.x, trans_pose.pose.position.y, trans_pose.pose.position.z]
            except:
                self.logger.logerr("Transform failed into ref_frame")
        self.__setpoint_msg.position = Point(*position)
        self.__setpoint_msg.velocity = Vector3(*velocity)
        self.__setpoint_msg.yaw = yaw
        self.lock_sp_msg.release()


    ## -- HELPER
    def transform(self, stamped_msg, ref_frame):
        TIMEOUT = 2 # [sec]
        rate = rospy.Rate(50)
        time = rospy.Time.now()
        #stamped_msg.header.stamp = stamped_msg.header.stamp - rospy.Duration(0.1) #for timing issues
        while not rospy.is_shutdown():
            try:
                tf_pose = self.__tfBuffer.transform(stamped_msg, ref_frame)
                break
            except tf2_ros.ExtrapolationException:
                dt = (rospy.Time.now() - time).to_sec()
                if dt > TIMEOUT:
                    raise Exception("Transform-Lookup Timeout!")
                rate.sleep()
            except:
                raise Exception("Transform-Lookup failed.")
        return tf_pose

    def generate_pose_stamped(self, position, yaw, ref_frame):
        header = Header(frame_id=ref_frame, stamp=rospy.Time.now())
        point = Point(*position)
        quat = tf.transformations.quaternion_from_euler(0, 0, yaw)
        quat = Quaternion(*quat)
        pose = Pose(position=point, orientation=quat)
        pose_stamped = PoseStamped(header=header, pose=pose)
        return pose_stamped
    
    ## -- GET FUNCTIONS
    def get_position(self):
        """Returns setpoint position

        Returns:
            numpy.ndarray : x, y, z (m)
        """
        pos = self.__setpoint_msg.position
        pos_array = np.array([pos.x , pos.y, pos.z])
        return pos_array

    def get_velocity(self):
        """Returns setpoint velocity

        Returns:
            numpy.ndarray : vx, vy, vz (m/s)
        """
        vel = self.__setpoint_msg.velocity
        vel_array = np.array([vel.x , vel.y, vel.z])
        return vel_array

    def get_mask_pos(self):
        mask = self.get_ignore_velocity() + self.get_ignore_acceleration() + self.get_ignore_yaw_rate()
        return mask

    def get_mask_pos_vel(self):
        mask = self.get_ignore_yaw_rate() + self.get_ignore_acceleration()
        return mask

    def get_mask_vel(self, z_velocity=True, yaw_rate=False):
        if z_velocity:
            mask = self.get_ignore_position() + self.get_ignore_acceleration()
        else:
            mask = self.get_ignore_acceleration() + self.get_ignore_position(z=False)
        if yaw_rate:
            mask += self.get_ignore_yaw()
        else:
            mask += self.get_ignore_yaw_rate()
        return mask

    def get_ignore_position(self, x=True, y=True, z=True):
        mask = 0
        if x:
            mask += PositionTarget().IGNORE_PX
        if y:
            mask += PositionTarget().IGNORE_PY
        if z:
            mask += PositionTarget().IGNORE_PZ
        return mask

    def get_ignore_velocity(self, x=True, y=True, z=True):
        mask = 0
        if x:
            mask += PositionTarget().IGNORE_VX
        if y:
            mask += PositionTarget().IGNORE_VY
        if z:
            mask += PositionTarget().IGNORE_VZ
        return mask

    def get_ignore_acceleration(self, x=True, y=True, z=True):
        mask = 0
        if x:
            mask += PositionTarget().IGNORE_AFX
        if y:
            mask += PositionTarget().IGNORE_AFY
        if z:
            mask += PositionTarget().IGNORE_AFZ
        return mask

    def get_force(self):
        mask = PositionTarget().FORCE
        return mask
    
    def get_ignore_yaw(self):
        mask = PositionTarget().IGNORE_YAW
        return mask

    def get_ignore_yaw_rate(self):
        mask = PositionTarget().IGNORE_YAW_RATE
        return mask