# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import numpy as np
import geonav_conversions as gc

import rospy
import tf
import tf2_ros

from timer import Timeout

## -- Messages
from std_msgs.msg import Header
from sensor_msgs.msg import Imu
from sensor_msgs.msg import NavSatFix
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import TransformStamped
from geometry_msgs.msg import Transform
from geometry_msgs.msg import Point
from geometry_msgs.msg import Vector3
from mavros_msgs.msg import Altitude


class LocalizationHandler:
    """Gets all telemetry data concerning the localization.

    Attributes:
        ns (str)                            : namespace of mavros instance
        logger (LogHandler())               : instance of LogHandler()
    """
    def __init__(self, ns, logger):
        ## -- Variables
        self.logger = logger
        self.__gp_fix = False
        self.__lp_fix = False
        self.__lp_msg = PoseStamped()
        self.__lp = np.array([.0, .0, .0])
        self.__lp_quat = np.array([.0, .0, .0, 1.0])
        self.__lv_msg = TwistStamped()
        self.__lv = np.array([.0, .0, .0])
        # you have to set the ref frame in your mavros config
        # without namespace "map" and with namspace to
        # "%s_map"%namespace
        if ns == "":
            self.__ref_frame = "map"
        else:
            self.__ref_frame = ns + "_map"

        ## -- TF
        self.__tfBuffer = tf2_ros.Buffer()
        self.__tfListener = tf2_ros.TransformListener(self.__tfBuffer)
        self._br = tf2_ros.TransformBroadcaster()

        ## -- SUBSCRIBER
        self.__pose_global_sub = rospy.Subscriber(ns + '/mavros/global_position/global', NavSatFix, self.__cb_global_position)
        self.__pose_local_sub = rospy.Subscriber(ns + '/mavros/local_position/pose', PoseStamped, self.__cb_local_pose)
        self.__vel_local_sub = rospy.Subscriber(ns + '/mavros/local_position/velocity_local', TwistStamped, self.__cb_local_velocity)
        self.__imu_sub = rospy.Subscriber(ns + '/mavros/imu/data', Imu, self.__cb_imu)
        self.__altitude_sub = rospy.Subscriber(ns + '/mavros/altitude', Altitude, self.__cb_altitude)
 
    ## -- CALLBACK FUNCTIONS
    ## -- NON PUBLIC
    def __cb_global_position(self, globalPos):
        """Callback of global position subscriber
        """
        if not self.__gp_fix:
            if not globalPos.latitude == 0.0:
                self.__gp_fix = True
                self.logger.loginfo("[GPS] ✓")
        self.__gp_msg = globalPos

    
    def __cb_local_pose(self, localPose):
        """Callback of local position subscriber
        """
        if not self.__lp_fix:
            self.__lp_fix = True
            self.logger.loginfo("[Local FIX] ✓")
        self.__ref_frame = localPose.header.frame_id
        self.__lp_msg = localPose
        self.__lp = np.array([localPose.pose.position.x,
                                     localPose.pose.position.y,
                                     localPose.pose.position.z])
        self.__lp_quat = np.array([localPose.pose.orientation.x,
                                      localPose.pose.orientation.y,
                                      localPose.pose.orientation.z,
                                      localPose.pose.orientation.w])


    def __cb_local_velocity(self, localVel):
        """Callback of velocity in reference frame 
        """
        self.__lv_msg = localVel
        self.__lv = np.array([localVel.twist.linear.x,
                                     localVel.twist.linear.y,
                                     localVel.twist.linear.z])


    def __cb_imu(self, imu):
        """Callback of orientation and acceleration
        """
        self.__orientation = np.array([imu.orientation.x, imu.orientation.y, imu.orientation.z, imu.orientation.w])
        self.__lin_accel = np.array([imu.linear_acceleration.x, imu.linear_acceleration.y, imu.linear_acceleration.z])


    def __cb_altitude(self, msg):
        """Callback of ASML altitude -> used for takeoff command
        """
        self.__amsl = msg.amsl

    ## -- GETTER FUNCTIONS
    def get_ref_frame(self):
        """Returns reference frame.

        Returns:
            str : reference_frame
        """
        return self.__ref_frame
        
    def has_gp_fix(self):
        """Returns GPS fix.

        Returns:
            bool : GPS fix
        """
        return self.__gp_fix

    def has_lp_fix(self):
        """Returns local position fix.

        Returns:
            bools : local position fix.
        """
        return self.__lp_fix

    def get_gp_msg(self):
        """Returns GPS message.
        
        Returns:
            sensor_msgs.msg.NavSatFix() : Global position message
        """
        return self.__gp_msg

    def get_lp_msg(self):
        """Returns local position message.
        
        Returns:
            geometry_msgs.msg.PoseStamped() : Local position message
        """
        return self.__lp_msg
    
    def get_asml(self):
        """Returns asml altitude.
        
        Returns:
            float : amsl altitude
        """
        return self.__amsl
    
    def get_lat_lon(self):
        """Returns latitude/longitude.

        Returns:
            numpy.ndarray : latitude, longitude
        """
        lat = self.__gp_msg.latitude
        lon = self.__gp_msg.longitude
        return np.array([lat, lon])

    def get_euler(self):
        """Returns euler from orientation.
        
        Returns:
            list : roll, pitch, yaw (rad)
        """
        return np.array(tf.transformations.euler_from_quaternion(self.__lp_quat))

    def get_roll(self):
        """Returns roll from orientation

        Returns:
            float : roll (rad)
        """
        return self.get_euler()[0]

    def get_pitch(self):
        """Returns pitch from orientation

        Returns:
            float : pitch (rad)
        """
        return self.get_euler()[1]

    def get_yaw(self):
        """Returns yaw from orientation

        Returns:
            float : yaw (rad)
        """
        return self.get_euler()[2]

    def get_position(self, ref_frame=None):
        """Returns cartisean coordinates to ref_frame.

        Arguments:
            ref_frame (str): reference frame

        Returns:
            numpy.ndarray : x, y, z (m)
        """
        if ref_frame is not None:
            pose = self.get_pose(ref_frame)
            x = pose.pose.position.x
            y = pose.pose.position.y
            z = pose.pose.position.z
            return np.array([x, y, z])
        else:
            return self.__lp

    def get_pose(self, ref_frame="map"):
        """Returns pose transformed into reference frame

        Arguments:
            ref_frame (str): reference frame

        Returns:
            geometry_msgs.msg.PoseStamped() : local position transformed into ref_frame
        """
        return self.transform(self.__lp_msg, ref_frame)

    def get_velocity(self):
        """Returns velocity in ref_frame (default='map').

        Returns:
            numpy.ndarray : vx, vy, vz (m/s)
        """
        return self.__lv

    def get_acceleration(self):
        """Returns acceleration in body frame.

        Returns:
            numpy.ndarray : ax, ay, az (m/s^2)
        """
        return self.__lin_accel

    def get_yaw_to_position(self, position):
        """Returns yaw to a given position.

        Arguments:
            position (numpy.ndarray, list, tuple): x,y (,z)
        """
        if type(position) == (tuple or list):
            position = np.array(position)
        elif type(position) == np.ndarray:
            delta = position - self.get_position()
            yaw = np.arctan2(delta[1], delta[0])
            return yaw
        else:
            raise TypeError('%s is not supported. Use tuple, list, numpy.ndarray' % type(position))

    def get_position_from_lat_lon(self, lat, lon, ref_frame=None):
        """Converts latitude/longitude to x/y in ref_frame.

        Arguments:
            lat     (float): latitude (deg)
            lon     (float): longitude (deg)
            ref_frame (str): reference frame

        Returns:
            numpy.ndarray : x, y (m)
        """
        position_copter = self.get_position(ref_frame)
        lat_lon_copter = self.get_lat_lon()
        lat_lon_local = gc.xy2ll(-position_copter[0], -position_copter[1], lat_lon_copter[0], lat_lon_copter[1])
        xy = gc.ll2xy(lat, lon, lat_lon_local[0], lat_lon_local[1])
        return np.array(xy)

    def get_lat_lon_from_position(self, position, ref_frame=None):
        """Converts x/y to latitude/longitude in ref_frame.

        Arguments:
            position (numpy.ndarray): x,y (,z) (m)

        Returns:
            numpy.ndarray : latitude, longitude (deg)
        """
        position_copter = self.get_position(ref_frame)
        lat_lon_copter = self.get_lat_lon()
        lat_lon_local = gc.xy2ll(-position_copter[0], -position_copter[1], lat_lon_copter[0], lat_lon_copter[1])
        lat_lon_pos = gc.xy2ll(position[0], position[1], lat_lon_local[0], lat_lon_local[1])
        return np.array(lat_lon_pos)

    def reached_position(self, pos, accuracy=0.5, ref_frame=None, sync=False, timeout=60):
        """Check if MAV reached position in reference frame.

        Arguments:
            pos         (numpy.ndarray) : position to be checked
            accuracy    (float)         : accuracy of check in m
            ref_frame   (str)           : reference frame for tf lookup
        
        Returns:
            bool: True if position is reached with accuracy in reference frame,
                  False otherwise
        """
        if sync:
            rate = rospy.Rate(20)
            t = Timeout(timeout)
            while not rospy.is_shutdown():
                if self.reached_position(pos, accuracy, ref_frame, sync=False):
                    return True
                if t.timed_out():
                    return False
                rate.sleep()
        else:
            if ref_frame is not None:
                pose_stamped = self.generate_pose_stamped(pos, 0, ref_frame)
                tf_pose = self.transform(pose_stamped, self.__ref_frame)
                pos = [tf_pose.pose.position.x, tf_pose.pose.position.y, tf_pose.pose.position.z]
            des_pos = pos
            cur_pos = self.__lp
            if np.linalg.norm(des_pos - cur_pos) <= accuracy:
                self.logger.loginfo("[POSITION] ✓")
                return True
            return False

    def reached_altitude(self, alt, accuracy=1, sync=False, timeout=60):
        """Check if MAV reached altitude in reference frame.

        Arguments:
            alt         (float) : altitude relative to reference frame.
            accuracy    (float) : accuracy of check in m
        
        Returns:
            bool: True if altitude is reached, False otherwise
        """
        if sync:
            rate = rospy.Rate(20)
            t = Timeout(timeout)
            while not rospy.is_shutdown():
                if self.reached_altitude(alt, accuracy, sync=False):
                    return True
                if t.timed_out():
                    return False
                rate.sleep()
        else:
            dist = np.abs(self.__lp[2] - alt)
            if dist <= accuracy:
                self.logger.loginfo("[ALTITUDE] ✓")
                return True
            return False

    ## -- HELPER FUNCTIONS
    def transform(self, stamped_msg, ref_frame):
        TIMEOUT = 2 # [sec]
        rate = rospy.Rate(50)
        time = rospy.Time.now()
        #stamped_msg.header.stamp = stamped_msg.header.stamp - rospy.Duration(0.1) #for timing issues
        while not rospy.is_shutdown():
            try:
                tf_pose = self.__tfBuffer.transform(stamped_msg, ref_frame)
                break
            except tf2_ros.ExtrapolationException:
                dt = (rospy.Time.now() - time).to_sec()
                if dt > TIMEOUT:
                    raise Exception("Transform-Lookup Timeout!")
                rate.sleep()
        return tf_pose

    def generate_pose_stamped(self, position, yaw, ref_frame):
        header = Header(frame_id=ref_frame, stamp=rospy.Time.now())
        pos = Point(*position)
        quat = tf.transformations.quaternion_from_euler(0, 0, yaw)
        quat = Quaternion(*quat)
        pose = Pose(position=pos, orientation=quat)
        pose = PoseStamped(header=header, pose=pose)
        return pose