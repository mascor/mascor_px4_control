# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import rospy

## -- Messages
from mavros_msgs.msg import State
from mavros_msgs.msg import ExtendedState

class StateHandler:
    """Handler for vehicle states.

    Attributes:
        ns        (str): mavros instance namespace
        logger (object): instance of LogHandler()
    """
    def __init__(self, ns, logger):
        ## -- Variables
        self.logger = logger
        self.__state = State(armed=False, mode="MANUAL")
        self.__extState = ExtendedState()
        ## -- Subscribers
        self.__state_sub = rospy.Subscriber(ns + '/mavros/state', State, self.__cb_state)
        self.__extstate_sub = rospy.Subscriber(ns + '/mavros/extended_state', ExtendedState, self.__cb_extended_state)


    ## -- CALLBACK FUNCTIONS
    def __cb_state(self, state):
        if self.__state.armed != state.armed:
            self.__state.armed = state.armed
            if state.armed == True:
                self.logger.loginfo("[ARMED]")
            else:
                self.logger.loginfo("[DISARMED]")
        if self.__state.mode != state.mode:
            self.__state.mode = state.mode
            s = "[FLIGHTMODE] " + str(state.mode)
            self.logger.loginfo(s)
        self.__state = state

    def __cb_extended_state(self, state):
        self.__extState = state
    

    ## -- CHECK FUNCTIONS
    def armed(self):
        """Check if MAV is armed.

        Returns:
            bool: True if armed, False otherwise
        """
        return self.__state.armed

    def landed(self):
        """Check if MAV is landed.

        Returns:
            bool: True if landed, False otherwise.
        """
        if self.__extState.landed_state == ExtendedState.LANDED_STATE_ON_GROUND:
            return True
        else:
            return False

    def in_air(self):
        """Check if MAV is in air.

        Returns:
            bool: True if in air, False otherwise.
        """

        if self.__extState.landed_state == ExtendedState.LANDED_STATE_IN_AIR:
            return True
        else:
            return False

    def takeoff(self):
        """Check if MAV takes off.

        Returns:
            bool: True if takes off, False otherwise.
        """

        if self.__extState.landed_state == ExtendedState.LANDED_STATE_TAKEOFF:
            return True
        else:
            return False

    def landing(self):
        """Check if MAV is landing.

        Returns:
            bool: True if lands, False otherwise.
        """

        if self.__extState.landed_state == ExtendedState.LANDED_STATE_LANDING:
            return True
        else:
            return False

    def is_offboard(self):
        """Check if flightmode is OFFBOARD mode.

        Returns:
            bool: True if offboard, False otherwise.
        """
        if self.__state.mode == "OFFBOARD":
            return True
        else:
            return False
    