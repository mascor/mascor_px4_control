# mascor_px4_control library
## author
Tobias Müller, B.Eng. - University of Applied Sciences Aachen
mail: tobias.mueller@fh-aachen.de
## info
This libray should simplify the control of the PX4 stack written in Python.

## included
- library (python)
- library testscript (lib_test.py)

## functions
- safety zone
- arm/disarm
- takeoff/land
- return to land
- go to position
- position queue
- position control
- position control + velocity feedforward
- velocity control (vx,vy,vz)/(vx,vy,z)

## howto

- simply import the library into your script with

```
from mascor_px4_control import MAV
```

- have a look into lib_test.py for examples

## todo (WIP)
- LICENCES file
- yaw to next position
- better multithreading

## roadmap
- handle px4 new offboard plans
- c++ implementation


# Documentation

It has following modules:

- [MAV(*ns*)](#mav) 
  - [LogHandler(mav_id)](#logger)
  - [PreflightHandler(*ns, logger*)](#preflight)
  - [StateHandler(*ns, logger*)](#state)
  - [CommandHandler(*ns, localization,  state,  logger*)](#commander)
  - [LocalizationHandler(*ns, logger*)](#localization)
  - [SetpointHandler(*ns, localization,  logger*)](#setpoint)
  - [MissionHandler(*ns*)](#mission)

## Example

This example is also find as node in the mascor_px4_control package. It shows a lot of the function from the library. Please have a look into the documentation for details.

```python
#!/usr/bin/env python
import sys

import rospy
from mascor_px4_control import MAV


def run_tests():
    rospy.init_node('mascorPX4controlTest')
    rospy.loginfo("mascor_px4_control libtest started ...")
    rate = rospy.Rate(1)
    ## -- create MAV object
    origin = [47.3977423, 8.5456063, 535.338898595]
    drone = MAV("")
    print("SET AUX CHANNEL")
    drone.command.set_aux_channel(1, 2000)
    ## -- generate mission
    drone.mission.clear()
    print("ADD WAYPOINT")
    drone.mission.add_waypoint(47.3977429, 8.5456595, 10)
    drone.mission.push()
    ## -- set home position at actual gps position
    print("SET HOME")
    drone.command.set_home(True)
    ## -- arm MAV
    print("ARM")
    drone.command.arm()
    print("WAIT FOR ARM")
    while not drone.state.armed():
        rate.sleep()
    ## -- takeoff to 5m
    print("TAKEOFF")
    drone.command.takeoff(5)
    print("WAIT FOR TAKEOFF")
    ## -- wait until altitude reached
    while not drone.localization.reached_altitude(5):
        rate.sleep()
    print("TAKEOFF #2")
    drone.command.takeoff(5)
    ## -- rotate +pi/2
    print("GET POSITION")
    pos = drone.localization.get_position()
    print("SETPOINT POSITION")
    drone.setpoint.position(pos, 3.1415)
    print("SET OFFBOARD")
    ## -- set flightmode to OFFBOARD
    while not drone.command.set_offboard():
        rate.sleep()
    rospy.sleep(3)
    print("SETPOINT GPS")
    drone.setpoint.gps(47.3976914, 8.5456483, 4, 3.1415)
    rospy.sleep(15)
    vel = [2,0,0]
    print("SETPOINT VELOCITY BODY FRAME")
    drone.setpoint.velocity(vel, frame=8)
    rospy.sleep(4)
    print("SETPOINT POSITION ACTUAL POSITION")
    drone.setpoint.position(drone.localization.get_position(), 1.57)
    rospy.sleep(4)
    vel = [2,0,0]
    print("SETPOINT VELOCITY BODY FRAME")
    drone.setpoint.velocity(vel, frame=8)
    # ## -- start mission
    drone.command.set_flightmode("AUTO.MISSION")
    while not drone.mission.reached_waypoint(0):
        rate.sleep()
    rospy.sleep(5)
    ## -- position setpoint
    print("SETPOINT POSITION")
    drone.setpoint.position([5,0,3])
    ## -- set flightmode to OFFBOARD
    print("SET OFFBOARD")
    while not drone.command.set_offboard():
        rate.sleep()
    ## -- wait until position reached
    print("WAIT FOR POSITION")
    while not drone.localization.reached_position([5,0,3]):
        rate.sleep()
    print("SETPOINT POSITION REF_FRAME")
    try:
        drone.setpoint.position([2,0,3], 1.57, "base_link")
        rospy.sleep(5)
    except:
        print("SETPOINT POSITION REF_FRAME FAILED!!!")
        pass
    ## -- velocity setpoint
    print("SETPOINT VELOCITY")
    drone.setpoint.velocity([0,0,2])
    rospy.sleep(2)
    print("LAND")
    drone.command.land()
    ## -- Node will be killed after copter has landed
    print("WAIT FOR LAND")
    while not rospy.is_shutdown():
        print("### waiting for landing ###")
        if drone.state.landed():
            break
        rate.sleep()
    rospy.signal_shutdown("end test")


if __name__ == '__main__':
    try:
        run_tests()
    except rospy.ROSInterruptException:
        sys.exit() 

```



## MAV(ns)<a name="mav"></a>

The MAV-class initializes all Handler and does a preflight check. MAV() is the standard to create an object to control a MAVROS-based UAV.

### Parameter

`ns -> str()` : MAVROS-namespace

### Functions

`get_id()`

- Returns:
  - `int()` : number of MAV. Everytime the same node creates a MAV() instance, the number increases.

`get_namespace()`

- Returns:
  - `str()` : MAVROS-namespace of MAV() instance.

`ready_for_takeoff()`

- Returns:
  - `bool()` : if MAV() is ready for takeoff. It makes an Pre-flight check if necessary

`is_killed()`

- Returns:
  - `bool()` : if MAV()-instance is deleted.

`delete()`

- Deletes MAV()-instance.

### Example

```python
mavs = list() 		#creates empty list
number_of_mavs = 3 	# number of MAVROS instances connected to UAVs

for i in range(number_of_mavs):
	mav = MAV("/uav_%s"%i) 		# namespace of the mavros instances is '/uav_0' etc.
	rospy.loginfo(mav.get_id()) # prints the ID of the MAV
	mavs.append(mav) 			# appends the mav to mavs list

for mav in mavs:
	mav.delete() # deletes the mav
```



## LogHandler(mav_id)<a name="logger"></a>

The LogHandler() appends the mav_id to the given string.

**You don't need it!**

### Parameter

`mav_id -> int()` : ID which is used to be printed



## PreflightHandler(*ns, logger*)<a name="preflight"></a>

The PreflightHandler() can check if necessary MAVROS-topics are publishing messages.

MAV() instance is doing this for you.

### Parameter

`ns -> str()` : MAVROS-namespace

`logger -> LogHandler()` : instance of LogHandler()

### Functions

`check()`

- Returns:
  - `bool()` : if MAV is ready for takeoff.



## StateHandler(*ns, logger*)<a name="state"></a>

Subscribes to state topics of mavros and makes them available.

### Parameter

`ns -> str()` : MAVROS-namespace

`logger -> LogHandler()` : instance of LogHandler()

### Functions

`armed()`

- Returns:
  - `bool()` : True if armed, False otherwise

`landed()`

- Returns:
  - `bool()` : True if landed, False otherwise.

`in_air()`

- Returns:
  - `bool()` : True if in air, False otherwise.

`takeoff()`

- Returns:
  - `bool()` : True if takes off, False otherwise.

`landing()`

- Returns:
  - `bool()` : True if lands, False otherwise.

`is_offboard()`

- Returns:
  - `bool()` : True if Offboard mode, False otherwise.



## CommandHandler(*ns, localization,  state,  logger*)<a name="commander"></a>

Provides functions to send commands to the px4 firmware.

### Parameter

`ns -> str()` : MAVROS-namespace

`localization -> LocalizationHandler()` : instance of LocalizationHandler()

`state -> StateHandler()` : instance of StateHandler()

`logger -> LogHandler()` : instance of LogHandler()

### Functions

`set_flightmode(flightmode)`

- Arguments:
  - `flightmode -> str()` :
    - MANUAL, ACRO, ALTCTL, POSCTL, OFFBOARD, STABILIZED, RATTITUDE, AUTO.MISSION, AUTO.RTL, AUTO.LAND, AUTO.RTGS, AUTO.READY, AUTO.TAKEOFF, AUTO.LOITER disable RC failsafe, which can be done by setting NAV_RCL_ACT parameter to 0

`set_offboard()`

- Sets flightmode to OFFBOARD mode

`set_home(curr_gps=True, lat=0., lon=0., alt=0.)`

- Arguments:
  - `curr_gps -> bool()` : True, sets actual position as home for return to home position
  - `lat, lon, alt -> float()` : latitude (deg), longitude (deg), altitude (m) if curr_gps == False

`set_aux_channel(channel, pwm)`

- Set AUX channel output to specified PWM value
- Arguments:
  - `channel -> int()` : AUX-Channel to set
  - `pwm -> int()` : PWM-value (1000-2000)

`return_to_home()`

- Sends RTL command

`arm()`

- Sends arm command

`disarm()`

- Sends disarm command

`takeoff(altitude, yaw=0.0)`

- Sends takeoff command with given altitude above actual ASML altitude
- **CAUTION!** Set actual yaw to avoid rotation while taking off
- Arguments:
  - `altitude -> float()` : altitude (m) above actual position
  - `yaw -> float()` : yaw (rad)

`land(altitude=0.0, yaw=0.0)`

- Sends land command with given altitude above starting point
- **CAUTION!** Set actual yaw to avoid rotation while landing

`get_param(param_name)`

- Arguments:
  - `param_name -> str()` : Firmware parameter name
- Returns:
  - `int()` : value of given firmware parameter



## LocalizationHandler(*ns, logger*)<a name="localization"></a>

Gets all telemetry data concerning the localization.

### Parameter

`ns -> str()` : MAVROS-namespace

`logger -> LogHandler()` : instance of LogHandler()

### Functions

`get_ref_frame()`

- Returns:
  - `str()` : reference frame

`has_gp_fix()`

- Returns:
  - `bool()` : GPS fix

`has_lp_fix()`

- Returns:
  - `bool()` : if EKF established local position

`get_gp_msg()`

- Returns:
  - `sensor_msgs.msg.NavSatFix()` : global position

`get_lp_msg()`

- Returns:
  - `geometry_msgs.msg.PoseStamped()` : local position

`get_asml()`

- Returns:
  - `float()` : ASML altitude (m)

`get_lat_lon()`

- Returns:
  - `numpy.ndarray([lat, lon])` : latitude (deg), longitude (deg)

`get_euler()`

- Returns:
  - `numpy.ndarray([r, p, y])` : roll (rad), pitch (rad), yaw (rad)

`get_roll()`

- Returns:
  - `float()` : roll (rad)

`get_pitch()`

- Returns:
  - `float()` : pitch (rad)

`get_yaw()`

- Returns:
  - `float()` : yaw (rad)

`get_position(ref_frame=None)`

- Returns:
  - `numpy.array([x, y, z])` : position (m) in reference frame

`get_pose(ref_frame="map")`

- Returns:
  - `geometry_msgs.msg.PoseStamped()` : in ref_frame transformed pose

`get_velocity()`

- Returns:
  - `numpy.ndarray([vx, vy, vz])` : velocity (m/s) in ENU-frame

`get_acceleration()`

- Returns:
  - `numpy.ndarray([ax, ay, az])` : acceleration (m/s^2) in body frame

`get_yaw_to_position(position)`

- Arguments:
  - `position -> numpy.ndarray([x, y (, z)])` : position to which you want to know the yaw
- Returns:
  - `float()` : yaw to given position

`get_position_from_lat_lon(lat, lon, ref_frame=None)`

- Arguments:
  - `lat, lon -> float()` : latitude, longitude (deg)
  - `ref_frame -> str()` : ref_frame=None if you want to get the position in local frame
- Returns:
  - `numpy.ndarray(x, y)` : position in ref_frame

`get_lat_lon_from_position(position, ref_frame=None)`

- Arguments:
  - `position -> numpy.ndarray([x, y (, z)])` : position (m) in ref_frame
  - `ref_frame -> str()` : reference frame for the given position, leave None if it is local frame of PX4
- Returns:
  - `numpy.ndarray(lat, lon)` : latitude, longitude (deg)

`reached_position(position, accuracy=0.5, ref_frame=None, sync=False, timeout=60)`

- Arguments:
  - `position -> numpy.ndarray([x, y, z])` : position (m) in ref_frame
  - `accuracy -> float()`: accuracy (m) to reach position
  - `ref_frame -> str()` : reference frame for the given position, leave None if it is local frame of PX4
  - `sync -> bool()` : hold until reached position or timeout
  - `timeout -> float()` : timeout for sync
- Returns:
  - `bool()` : True if actual position is in accuracy range, False otherwise

`reached_altitude(altitude, accuracy=1.0, sync=False, timeout=60)`

- Arguments:
  - `altitude -> float()`: altitude (m) to reach
  - `accuracy -> float()` : accuracy (m) to reach altitude
  - `sync -> bool()` : hold until reached altitude or timeout
  - `timeout -> float()` : timeout for sync
- Returns:
  - `bool()` : True if actual altitude is in accuracy range, False otherwise



## SetpointHandler(*ns, localization, logger*)<a name="setpoint"></a>

Handler for setpoints in offboard flightmode.

### Parameter

`ns -> str()` : MAVROS-namespace

`localization -> LocalizationHandler()` : instance of LocalizationHandler()

`logger -> LogHandler()` : instance of LogHandler()

### Functions

`hold()`

- Set actual position for Offboard mode.

`gps(lat, lon, z, yaw=0.0)`

- Set GPS for Offboard mode.
- Arguments:
  - `lat, lon -> float()` : latitude, longitude (deg)
  - `z -> float()` : altitude (m) in local frame
  - `yaw -> (float)` : yaw (rad)

`position(position, yaw=0.0, ref_frame=None)`

- Set position for Offboard mode.

- Arguments:
  - `position -> numpy.ndarray([x, y, z])` : position (m) in ref_frame
  - `yaw -> float()` : yaw (rad) 
  - `ref_frame -> str()` : name of reference frame

`velocity(velocity, z=0.0, yaw=0.0, yaw_rate=0.0, frame=PositionTarget().FRAME_LOCAL_NED)`

- Set Velocity for Offboard mode.
- Arguments:
  - `velocity -> numpy.ndarray([vx, vy, vz])` :  velocity (m/s)
  - `z -> float()` : altitude (m)
  - `yaw -> float()` : yaw angle (rad)
  - `yaw_rate -> float()` : yaw rate (rad/s)
  - `frame -> int()`:
    - `PositionTarget().FRAME_LOCAL_NED`
    - `PositionTarget().FRAME_BODY_NED`

`position_and_velocity(position, velocity, yaw=0.0, ref_frame=None)`

- Set Position with velocity as feedforward for offboard mode.
- Arguments:
  - `position -> numpy.ndarray([x, y, z])` : position (m)
  - `velocity -> numpy.ndarray([vx, vy, vz])` :  velocity (m/s)
  - `yaw -> float()` : yaw angle (rad)
  - `ref_frame -> str()` : reference frame for the position

`get_position()`

- Returns:
  - `numpy.ndarray([x, y, z])` : setpoint position (m)

`get_velocity()`

- Returns:
  - `numpy.ndarray([vx, vy, vz])` : setpoint velocity (m/s)



## MissionHandler(*ns*)<a name="mission"></a>

Handler for missions.

### Parameter

`ns -> str()` : MAVROS-namespace

### Functions

`pull()`

- Requests waypoints from device.
- Returns:
  - `mavros_msgs.msg.WaypointPullResponse()` : success status and received count

`push(start_seq=0, waypoint_list=None)`

- Send waypoints to device.
- Arguments:
  -  `start_seq -> int()` : will define a partial waypoint update. Set to 0 for full update
  - `waypoint_list -> list(*mavros_msgs.msg.Waypoint())` : list of waypoints
- Returns:
  - `mavros_msgs.msg.WaypointPushResponse()` : response from waypoint push service

`clear()`

- Request clear waypoint.

`set_current(seq):`

- Request set current waypoint.
- Arguments:
  - seq (int): index in waypoint array
- Returns:
  - bool : success

`add_waypoint(lat, lon, alt,seq=-1,frame=Waypoint.FRAME_GLOBAL_REL_ALT, command=CommandCode.NAV_WAYPOINT,autocontinue=True, param1=0.0, param2=0.0, param3=0.0,param4=0.0))`

- Add waypoint to waypoint list.
- Arguments:
  - `lat, lon -> float()` : latitude, longitude (deg)
  - `alt -> float()` : altitude relative to frame(m)
  - `seq -> int()` : replace waypoint at this position
  - `frame -> int()` : http://docs.ros.org/melodic/api/mavros_msgs/html/msg/Waypoint.html
  - `command -> int()` : http://docs.ros.org/melodic/api/mavros_msgs/html/msg/CommandCode.html
  - `autocontinue -> bool()` : TODO
  - `param1-4 -> float()`: https://mavlink.io/en/messages/common.html#MAV_CMD_NAV_WAYPOINT

`delete_waypoint(seq)`

- Deletes waypoint from list. **Has to be pushed again!**
- Arguments:
  - `seq -> int()`: waypoint from list at position seq will be deleted

`get_waypoint_list()`

- Returns waypoint list message.
- Returns:
  - `mavros_msgs.msg.WaypointList()` : list of waypoints

`get_reached_msg()`

- Returns waypoint reached message.
- Returns:
  - `mavros_msgs.msg.WaypointReached()` : reached waypoint message

`reached_waypoint(seq)`

- Returns if waypoint is reached.
- Arguments:
  - `seq -> int()`: waypoint sequence id
- Returns:
  - `bool()` : True if waypoint sequence id is reached, False otherwise
